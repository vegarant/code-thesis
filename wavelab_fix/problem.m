
vanishing_moments = 2;
V_0 = 3;

n = 2^4;

% Linear polynomial
x = linspace(1,n,n)';

wc = FWT_CDJV(x, V_0, vanishing_moments);

% These two coefficients should be zero if the boundary wavelets has 2 
% vanishing moments
wc(9:10)


