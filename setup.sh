#! /bin/bash

echo "Creating all necessary subdirectories"
mkdir -p practical_experiments/plots
mkdir -p figures/asymCS/plots
mkdir -p figures/coherence/plots
mkdir -p figures/hadam/plots
mkdir -p figures/intro/plots
mkdir -p figures/wave/plots


echo "Creating figures to chapter 1"
cd figures/intro
python2 sampling_mat_hadam.py 1
python2 sampling_mat_hadam.py 4

echo "Creating figures to chapter 3"
cd ../wave
python2 plotDB_wavelets.py
python2 waveplot.py


echo "Creating figures to chapter 4"
cd ../hadam
python2 tikzhadam.py
python2 visualize_matrix.py

echo "Creating figures to chapter 5"
cd ../asymCS
python2 fourier_and_hadam_domain.py
python2 sampling_matrices.py
python2 sampling_mat_hadam.py 1 sequency
python2 sampling_mat_hadam.py 1 hadamard
python2 d2DWT.py > /dev/null
python2 asymSparsityDWT.py 1

echo "Creating figures to chapter 6"
cd ../coherence
python2 read_file_coherence.py 2 16 0
python2 read_file_coherence.py 2 16 1
python2 read_file_coherence.py 3 16 0
python2 read_file_coherence.py 3 16 1

python2 read_file_max_elem.py 2 16 0
python2 read_file_max_elem.py 2 16 1
python2 read_file_max_elem.py 3 16 0
python2 read_file_max_elem.py 3 16 1


