# Master thesis code - Vegard Antun

## About
This repository contain all the code used to produce the images found in my 
master thesis. It is structured as follows:

* __figures__ - This directory along with its subdirectories contain the scripts 
            used to produce most of the images.
* __optimization_alg__ - Contain an implementation of five L1-optimization solvers.
* __practical_experiments__ - Contain all of the Matlab scripts related to 
                              compressive sensing.
* __wavelab_fix__ - A fix of the boundary wavelet pre- and postconditioning 
                    matrices found in the Wabelab package.

All of the code have only been tested on a Linux system.

## Create figures
To create most of the figures included in the thesis run the bash script
`setup.sh`. For some of the images, the data needed in the plot takes a
considerable amount of time to generate. In such cases some of the data have
been precomputed and written to files. This is also the case for all data
created by Matlab scripts. 

The Matlab scrips found in the practical_experiments directory, can reproduce
any of the reconstructed images seen in the thesis. It will, however, for some 
of the plots, be required that one changes the input file,
sampling operator, number of vanishing moments, or sampling rate. All of these 
options are placed on the top of each file. To make it easy to change between
the different sampling operators, all of them have been placed in separate
classes, with a unified interface. Thus, to change the operator should only
require that one change one single line.


## Dependencies
To make all of this code work correctly the following libraries must be 
installed.

* [Fastwht](https://bitbucket.org/vegarant/fastwht/) - My implementation of the Walsh Hadamard transform.
* [Ryan's Wavelet libary](http://folk.uio.no/oyvindry/matinf2360/code/).
* [SPGL1](http://www.cs.ubc.ca/labs/scl/spgl1) - The Matlab SPGL1 solver.
* [Wavelab](http://statweb.stanford.edu/~wavelab/) - To make everything work correctly my fix to the code must be applied. 

In addition it is assumed that the following is installed: Matlab, Python 2,
Matplotlib, NumPy. If you encounter other dependencies not listed here, please
notify me. 

## See also
[fastwht](https://bitbucket.org/vegarant/fastwht/), [SPGL1](https://bitbucket.org/vegarant/spgl1).

## Contact
For any questions concerning the code please contact Vegard Antun at 
vegarant@student.matnat.uio.no