


function y = fourier2IDB2d(x, mode,idx, nres, size_dim1)
    
    if mode == 1 
         
        z = reshape(x, size_dim1, size_dim1); 
        z = IDWT2Impl(z, nres, @IDWTKernelOrtho);
        z = fftshift(fft2(z))/size_dim1;
        y = z(idx);
         
    else % Transpose
         
        z = zeros(size_dim1, size_dim1);
        z(idx) = x;
        z = ifft2(ifftshift(z))*size_dim1;
        z = DWT2Impl(z, nres, @DWTKernelOrtho); 
        y = reshape(z, size_dim1*size_dim1, 1);
         
    end
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
end




