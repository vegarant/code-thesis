% This scripts performs the flip test on the specified image using the specified
% transform.


filename = 'pictures/klubbe1024.png';
subsamplingRate = 0.10;             % M = round(N*subsamplingRate)
vm    = 4;                          % Number of vanishing moments
sigma = 100;                        % Minimize ||Ax-b||_2 subject to ||x||_1 ≤ sigma
ft    = FT_Fourier2d_circ;          % Choose fast transform
hadamardOrder = 'sequency';

% Read image and convert it to a vector
im = double(imread(filename));
[im_rows, im_cols] = size(im);
N = im_rows*im_cols;
M = round(N*subsamplingRate);

ft.M = M;
ft.N = N;
ft.rows = im_rows;
ft.cols = im_cols;
ft.vm = vm;
ft.nres = floor(log2(im_rows) + 1e-8);

liftingfactortho(vm);

ft.create_sampling_scheme();

% Read image and convert it to a vector
im = double(imread(filename));
[im_rows, im_cols] = size(im);
N = im_rows*im_cols;
M = round(N*subsamplingRate);

im_dwt    = DWT2Impl(im, ft.nres, @DWTKernelOrtho);
if ft.is_fourier
    im_dwt_fliped = ifftshift(im_dwt);
else 
    im_dwt_fliped = fliplr(flipud(im_dwt));
end
im_fliped = IDWT2Impl(im_dwt_fliped, ft.nres, @IDWTKernelOrtho);

b = ft.sampling_operator(im);  
b_fliped = ft.sampling_operator(im_fliped);  

opA = @(x, mode) ft.sampling_and_inverse_wavelet_operator(x, mode);

opts = spgSetParms('verbosity',1);
z = spg_bpdn(opA, b, sigma, opts); %  minimize ||x||_1  s.t.  ||Ax - b||_2 <= sigma
z_fliped = spg_bpdn(opA, b_fliped, sigma, opts); %  minimize ||x||_1  s.t.  ||Ax - b||_2 <= sigma

% Reconstruct image 
z1 = reshape(z, im_rows, im_cols);
im_rec = IDWT2Impl(z1, ft.nres, @IDWTKernelOrtho);

% Reconstruct fliped image
z1_fliped = reshape(z_fliped, im_rows, im_cols);

if ft.is_fourier
    z1_unfliped = ifftshift(z1_fliped);
else 
    z1_unfliped = flipud(fliplr(z1_fliped));
end

im_rec_fliped = IDWT2Impl(z1_unfliped, ft.nres, @IDWTKernelOrtho);


%im_rec = 255*(im_rec - min(min(im_rec)))/max((max(im_rec)) - max(min(im_rec)));
%im_rec = uint8(reshape(im_rec, im_rows, im_cols));

if  (~exist('rec_cnt'))
    rec_cnt = 1;
end

if ft.is_fourier
    opStr = 'fourier';
else 
    opStr = 'hadam';
end


filename_unfliped = sprintf('plots/rec%d_dim_%d_subs_%d_sig_%d_op_%s_unfliped.png', ...
                            rec_cnt, ft.rows, 100*subsamplingRate, sigma, opStr);
filename_fliped = sprintf('plots/rec%d_dim_%d_subs_%d_sig_%d_op_%s_fliped.png', ...
                            rec_cnt, ft.rows, 100*subsamplingRate, sigma, opStr);
imwrite(uint8(im_rec),  filename_unfliped);
imwrite(uint8(im_rec_fliped),  filename_fliped);
fprintf('Saving image as %s\n', filename_unfliped);
fprintf('Saving image as %s\n', filename_fliped);


% Save sampling pattern
Y = zeros(ft.rows,ft.cols);
Y(ft.idx) = 255;
filename_samples = sprintf('plots/sp_flip_rec%d_%s_dim_%d_subs_%d.png',...
                            rec_cnt, opStr, ft.rows,100*subsamplingRate );
imwrite(uint8(Y), filename_samples);
fprintf('Saving sampling pattern as %s\n', filename_samples);

rec_cnt = rec_cnt + 1;


