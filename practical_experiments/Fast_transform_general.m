% Fast_transfrom_general is the superclass defining an interface for all fast 
% transforms.
% 
% 
classdef (Abstract) Fast_transform_general < handle
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%                      Static validation functions                 %%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods (Static, Access = protected )
        % This function tests if `value` is a power of 2. It `value` is not, an 
        % value_exception is thrown. If is a power of 2, nothing happens.
        function is_power_of_2(value)     
            Fast_transform_general.is_positive(value);
            nu = floor(log2(value) + 1e-8);
            bool = 2^nu == value;
            if (2^nu ~= value)
                warning('is_power_of_2: Value must be power of 2');
            end 
        end
        
        function is_positive(value)
            if (value <= 0)
                warning('is_positive: value must be positive');
            end
        end
        
        % Issue a warning if `value` have not been initialized
        function is_initialized(value, msg);
            if (isempty(value))
                warning(msg);
            end
        end
        
    end
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%                           Properties                             %%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    properties 
          
        M;      % Number of samples.
        N;      % Length of one-dimensional signal.
        idx;    % The chosen sampling indexes.
        vm;     % Number of vanishing moments for the wavelet.
        nres;   % Number of wavelet decompositions.
    
    end 
    
    properties (Abstract)
        is_fourier; % bool, wheter or not the sampling operator is a fourier 
                    % operator 
    end 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%                      Interface functions                         %%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods (Abstract)
        
        % Create sampling scheme creates a sampling scheme, choosing which rows
        % one would like to sample. This function need to be called before the 
        % two operator functions. 
        obj = create_sampling_scheme(obj); 
        
        % This function contains the operator where the actual sampling will 
        % take place. It will return the measurement vector `b`
        %
        % INPUT:
        % x - signal
        b = sampling_operator(obj, x) 
        
        % This function will correspond to the sampling operator multiplied by 
        % the inverse wavelet transform.
        %
        % INPUT:
        % x    - Signal
        % mode - bool. If false the transpose of the operator will be applied.
        y = sampling_and_inverse_wavelet_operator(obj, x, mode);
    end
    

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%                     Set and get functions                        %%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %
    % These functions will issue a warning if wrong input is applied.
    %
    methods 
         
        function obj = set.N(obj,N) 
            Fast_transform_general.is_power_of_2(N);
            obj.N = N;
        end
         
        function obj = set.vm(obj,vm) 
            Fast_transform_general.is_positive(vm);
            obj.vm = vm;
        end
         
        function obj = set.M(obj,M)
            Fast_transform_general.is_positive(M);
            obj.M = M;
        end
        
        function obj = set.idx(obj, idx)
            obj.idx = idx;
        end
         
        function obj = set.nres(obj, value)
            Fast_transform_general.is_positive(value);
            obj.nres = value;    
        end
         
         
    end % end of methods 
    
    

     
end % end of class
