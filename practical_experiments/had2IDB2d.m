% This function implements the matrix product between a hadamard matrix and 
% an inverse daubechies wavelet matrix. The matrix produts are extended to two 
% dimensions using the tensor product. That is a 2^R × 2^R dimensional signal X
% will be multiplied as
%                            HAD*IDWT*X*(IDWT)^(T)*(HAD)^(T) 
% Where HAD is the hadamard matrix, IDWT is the inverse discrete wavelet 
% transform working column vectors of X. If mode is set not equal to 1 the 
% transpose of the matrices will be applied. That is 
%                            DWT*HAD*X*(HAD)^(T)*(DWT)^(T)
% The argument `idx` chooses which of the transfomed indices which should be 
% sampled. `idx` is the linear indices of the matrix, see `sub2ind` function
% for conversion to this type of indices.
%
% INPUT:
% x    - one dimensional vector. The vector will be reshaped to the two 
%        dimensional matrix X.
% mode - If mode is not equal to 1, the transpose matrix product will be applied. 
%        Otherwise the usual matrix product will be applied.
% idx  - Linear indices of the samples one would like to obtain.  
% nres - Number of wavelet resulution spaces.
% size_dim1     -  Size along one of the two dimensions of the matrix. That is 
%                  if the two dimensional signal has size 2^R × 2^R, then 
%                  size_dim1 = 2^R.
% hadamard_order - The order of the hadamard matrix. Can be one of 'hadamard', 
%                  'sequency' or 'dyadic'.
%
% OUTPUT:
% y - The two dimensional signal, reshaped into a vector.
%
% SEE ALSO:
% `sub2ind`, `fastwht`, `fastwht2d`, `DWT2Impl` and `IDWT2Impl`.
%
function y=had2IDB2d(x, mode, idx, nres, size_dim1,  hadamard_order)
     
    nrb_samples = size(idx, 1);
     
    if mode == 1 
         
        z = reshape(x, size_dim1, size_dim1); 
        z = IDWT2Impl(z, nres, @IDWTKernelOrtho);
        z = fastwht2d(z, hadamard_order)*size_dim1;
        y = z(idx);
         
    else % Transpose
         
        z = zeros(size_dim1, size_dim1);
        z(idx) = x;
        z = fastwht2d(z, hadamard_order)*size_dim1;
        y = DWT2Impl(z, nres, @DWTKernelOrtho); 
        y = reshape(y, size_dim1*size_dim1, 1);
    end




