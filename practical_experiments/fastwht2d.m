% Applies the two dimensional fast Walsh-Hadamard transform to a two dimensional 
% signal, by a tensor product expansion. 


function Y = fastwht2d(X, order)
    eps = 1e-9; 
        
    if (nargin == 1)
        order = 'sequency';
    end
        
    [M, N] = size(X);
         
    % test that size is a power of 2
    nu_m = log2(M);
    nu_n = log2(N);
    assert (abs(round(nu_m) - nu_m) < eps), 'Image size is not a power of 2';
    assert (abs(round(nu_n) - nu_n) < eps), 'Image size is not a power of 2'; 
         
    Y = X;
    for i = 1:M
        Y(:,i) = fastwht( X(:,i), [], order );
    end  
    for i = 1:N
        Y(i,:) = fastwht( Y(i,:), [], order );
    end  
         
end





