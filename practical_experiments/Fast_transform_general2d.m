% Fast_transfrom_general2d extends the Fast_transform_general class by the 
% properties `rows` and `cols`.  
classdef Fast_transform_general2d < Fast_transform_general 
     
    properties
        rows;
        cols;
    end
     
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%                     Set and get functions                        %%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods
        function obj = set.rows(obj, value)
            Fast_transform_general.is_power_of_2(value);
            obj.rows = value;
        end
         
         
        function obj = set.cols(obj, value)
            Fast_transform_general.is_power_of_2(value);
            obj.cols = value;
        end
         
         
         
         
    end % end methods

end % end class



