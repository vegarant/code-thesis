from hadamard import fastwht, PAL, WAL; 
from numpy import *;
import matplotlib.pyplot as plt;
import matplotlib.cm as cm

"""
Plot the Hadamard matrices as images.
"""

N = 32;
dest = 'plots/';

X = zeros([N,N]);

# Sequency and hadamard ordering
for order in ['sequency', 'hadamard']:
    for i in range(N):
        ei = zeros([N]); ei[i] = 1;
        ei = fastwht(ei, order=order);
        X[:,i] = ei;
    #print X
    fig = plt.matshow(X, cmap=cm.gray);
    fig.axes.get_xaxis().set_visible(False);
    fig.axes.get_yaxis().set_visible(False);
    plt.savefig(dest + "%s_N_%d.png" % (order, N), dpi=150, transparent=True, bbox_inches='tight', pad_inches=0);

# Paley enumeration
for n in range(N):
    for t in range(N):
        X[n,t] = PAL(N,n,t);

fig = plt.matshow(X, cmap=cm.gray);
fig.axes.get_xaxis().set_visible(False);
fig.axes.get_yaxis().set_visible(False);
plt.savefig(dest + "paley_N_%d.png" % (N), dpi=150, transparent=True, bbox_inches='tight', pad_inches=0);



