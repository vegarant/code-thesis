
% Multiplies the Hadamard matrix with a wavelet matrix and writes the maximum
% element in each row and column to a file.
% NOTE: It will only work if one uses the files with the fix I provided for the
%       Wavelab wavelet libary. 


vm = 3;
nu = 16;
N  = 2^nu;
lwidth = 2;

dest_data = 'data/';

liftingfactortho(vm);
max_row = zeros(N,1);
max_col = zeros(N,1);

V_0 = 3;
tic;
for i = 1:N
    ei = zeros(N,1); ei(i) = 1;
    wc = IWT_CDJV_no_postcon(ei, V_0, vm);    
    %wc = IDWTImpl(ei, nu-3, @IDWTKernelOrtho);
    x  = fastwht(wc, N, 'dyadic');
    x  = abs(sqrt(N)*x);
    max_elem   = max(x);
    max_col(i) = max_elem;    
    idx = x > max_row;
    max_row(idx) = x(idx);
     
     
end
idx = zeros(N,1);
n=0;
while(n < nu)
    idx(2^n:2^(n+1)+1) = 2^(-n+2);
    n = n + 1;
end
filename = sprintf('%shadamard_DB_%d_dim_%d_bd_no_precon_paley.txt', dest_data, vm, nu)
fileID = fopen(filename, 'w');
for i = 1:N
    fprintf(fileID, '%19.16e %19.16e\n', max_col(i), max_row(i));
end




