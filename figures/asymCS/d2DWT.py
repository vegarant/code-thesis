from numpy import *
import matplotlib.pyplot as plt
from dwt import DWTImpl, IDWTImpl, DWT2Impl, IDWT2Impl, DWTKernelOrtho, IDWTKernelOrtho, DWTKernelHaar, IDWTKernelHaar, liftingfactortho
import matplotlib.cm as cm
from mpl_toolkits.axes_grid1 import make_axes_locatable;

"""
Plots a tree level two-dimensional wavelet transform of the image.
"""



filename = 'klubbe1024.png';
vm = 1;
dest = 'plots/'
fsize = 30;


set_printoptions(linewidth=120);


im = asarray(plt.imread(filename), dtype=float64);
im = (im - amin(im))/(amax(im) - amin(im));

liftingfactortho(vm);

nres = 3;

im_dwt = im.copy();
DWT2Impl(im_dwt, nres, DWTKernelOrtho);
#im_dwt = (im_dwt - amin(im_dwt))/(amax(im_dwt) - amin(im_dwt));

M,N = im_dwt.shape;
im1 = abs(im_dwt[M/2:M, 0:M/2]);
im2 = abs(im_dwt[M/8:M/4, 0:M/8]);
im3 = abs(im_dwt[0:M/8, 0:M/8]);

print amax(im);
print amin(im);

th = 0.06;
s1 = sum(im1>th);
s2 = sum(im2>th);
s3 = sum(im3>th);

k1 = float((M/2)*(M/2));
k2 = float((M/8)*(M/8));

print 's1: %5d, size: %7d, ratio: %g %%' % (s1, (M/2)*(M/2), 100*s1/k1);
print 's2: %5d, size: %7d, ratio: %g %%' % (s2, (M/8)*(M/8), 100*s2/k2);
print 's3: %5d, size: %7d, ratio: %g %%' % (s3, (M/8)*(M/8), 100*s3/k2);



#plt.imshow(abs(im1));
#plt.colorbar();
#plt.show();

plt.imsave(dest + "d2DWT_DB_%d_klubbe.png" % (vm), im_dwt, cmap=cm.gray);

im1.shape = (im1.shape[0]*im1.shape[1]);
im2.shape = (im2.shape[0]*im2.shape[1]);

plt.figure();
plt.plot(im1,'k');
plt.axis([0,len(im1), 0, 3]);
plt.xticks([0, len(im1)/2, len(im1)-11000], ["0", "$2^{%d}$" %(int(log2(len(im1)/2))), "$2^{%d}$" %(int(log2(len(im1))))], fontsize=fsize);
plt.yticks(fontsize=fsize);
plt.savefig(dest + 'd2DWT_DB_%d_klubbe_red_box.png' % (vm), dpi=150, \
            transparent=False, bbox_inches='tight');

plt.figure();
plt.plot(im2,'k');
plt.axis([0,len(im2), 0, 3]);
plt.xticks([0, len(im2)/2, len(im2)-700], ["0", "$2^{%d}$" %(int(log2(len(im2)/2))), "$2^{%d}$" %(int(log2(len(im2)))) ], fontsize=fsize);
plt.yticks(fontsize=fsize);
plt.savefig(dest + 'd2DWT_DB_%d_klubbe_blue_box.png' % (vm), dpi=150, \
            transparent=False, bbox_inches='tight');

#plt.figure();    
#ax = plt.gca();
#im = ax.matshow(im, cmap=cm.gray);
#plt.xticks([]);
#plt.yticks([]);
#divider = make_axes_locatable(ax)
#cax = divider.append_axes("right", size="5%", pad=0.05)
#cbar = plt.colorbar(im, cax=cax);
#cbar.set_ticks([0,0.25,0.5,0.75,1]);
#cbar.set_ticklabels([0,0.25,0.5,0.75,1]);
#cbar.ax.tick_params(labelsize=fsize);
#plt.show();
#plt.savefig("%s/d2DWT_DB_%d_klubbe.png" % (dest, vm), dpi=150, \
#            transparent=False, bbox_inches='tight');











