from numpy import *;
import matplotlib.pyplot as plt;
from mpl_toolkits.axes_grid1 import make_axes_locatable;
from hadamard import fastwht;
from dwt import *

"""
Plots the image and the dwt-fliped image in both the Fourier and Hadamard domain.
"""



src_original = './'
dest = 'plots/';

nu = 10;
dim = 2**nu;
vm = 4;
liftingfactortho(vm);


filename_orig = 'klubbe%d.png' % (dim);

im_orig = asarray(plt.imread(src_original + filename_orig), dtype = float64);
im_rows, im_cols = im_orig.shape;

im_fft = fft.fftshift(fft.fft2(im_orig))/im_rows;

im_had = im_orig.copy();


for i in range(im_cols):
    im_had[:,i] = fastwht(im_had[:,i])*sqrt(im_rows);
for i in range(im_rows):
    im_had[i,:] = fastwht(im_had[i,:])*sqrt(im_cols);

plt.imsave('plots/klubbe%d_fft.png' % dim, log(abs(im_fft)), cmap=plt.cm.jet);
plt.imsave('plots/klubbe%d_had.png' % dim, log(abs(im_had)), cmap=plt.cm.jet);

im_dwt = im_orig.copy(); 

DWT2Impl(im_dwt, nu, DWTKernelOrtho, False); 

im_dwt_fliped = flipud(fliplr(im_dwt));

IDWT2Impl(im_dwt_fliped, nu, IDWTKernelOrtho, False);

plt.imsave('plots/klubbe%d_fliped.png' % dim, im_dwt_fliped, \
                                                               cmap=plt.cm.gray);

im_dwt_fft = fft.fftshift(fft.fft2(im_dwt_fliped))/im_rows;

plt.imsave('plots/klubbe%d_fliped_fft.png'%(dim), log(abs(im_dwt_fft)), \
                                                               cmap=plt.cm.jet);

for i in range(im_cols):
    im_dwt_fliped[:,i] = fastwht(im_dwt_fliped[:,i])*sqrt(im_rows);
for i in range(im_rows):
    im_dwt_fliped[i,:] = fastwht(im_dwt_fliped[i,:])*sqrt(im_cols);

plt.imsave('plots/klubbe%d_fliped_had.png'%(dim), log(abs(im_dwt_fliped)), \
                                                               cmap=plt.cm.jet);


N = 1000;
M = 50;
X = zeros([M,N]);
y = linspace(0,1,N);
for i in range(M):
    X[i,:] = y[:].copy();

plt.matshow(X, cmap=plt.cm.jet);
plt.yticks([])
diff = 90;
plt.xticks([diff, N-diff], ['Low values', 'High values'], fontsize=25);
plt.savefig(dest + 'jetColorspace.png', dpi=150, transparent=True, \
            bbox_inches='tight');
#plt.imsave('plots/klubbe%d_had_dwt.png' % dim, logabs(im_had), cmap=plt.cm.jet);


