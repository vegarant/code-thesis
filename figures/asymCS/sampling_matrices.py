from numpy import *
from hadamard import fastwht
from dwt import DWTImpl, IDWTImpl, DWTKernelOrtho, IDWTKernelOrtho, DWTKernelHaar, IDWTKernelHaar, liftingfactortho;
import matplotlib.pyplot as plt;
import matplotlib.cm as cm;
from mpl_toolkits.axes_grid1 import make_axes_locatable;
import os;
import sys;
set_printoptions(linewidth=120);

"""
Plot the Hadamard matrices multiplied with a IDWT matrix and a DFT matrix 
multiplied with a IDWT matrix. From the resulting matrix one extracts one column 
from each of the tree rightmost blocks. 
"""


def plot_matrices_and_blocs(samp_operator, vm_list, nu, is_fourier=True):
    if is_fourier:
        name = 'fourier';
    else:
        name = 'hadamard';
    
    fsize = 25;
    lwidth = 3;
    
    block1 = [];
    block2 = [];
    block3 = [];
    
    nu = 10;
    
    dest = 'plots/';
    nres = nu;
    N    = 2**nu;
    
    for vm in vm_list:
        
        liftingfactortho(vm);
        
        U = zeros([N, N], dtype=complex128);
        for i in xrange(N):
            ei = zeros(N); ei[i]=1;
            IDWTImpl(ei, nres, IDWTKernelOrtho,False); 
            ei = samp_operator(ei);
            U[:,i] =  ei;
        
        block1.append(U[:,N/2+N/4]);
        block2.append(U[:,N/4+N/8]);
        block3.append(U[:,N/8+N/16]);
        
        
        plt.figure();    
        ax = plt.gca();
        im = ax.matshow(abs(U), cmap=cm.jet);
        plt.xticks([]);
        plt.yticks([]);
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="5%", pad=0.05)
        cbar = plt.colorbar(im, cax=cax);
        cbar.set_ticks([0,0.25,0.5,0.75,1]);
        cbar.set_ticklabels([0,0.25,0.5,0.75,1]);
        cbar.ax.tick_params(labelsize=fsize);
        
        plt.savefig(dest + "%s_DB_%d_.png" % (name,vm), dpi=150, \
                    transparent=False, bbox_inches='tight');
    
    
    k = 1;
    for block in [block1, block2, block3]:
        plt.figure();
        ax = plt.gca();
        ax.yaxis.tick_right()
        i = 0;
        leg = [];
        for vm in vm_list:
            
            plt.plot(abs(block[i]), linewidth=lwidth);
            plt.hold('on');
            leg.append('DB%d' % vm);
            if is_fourier:
                plt.xticks([0,N/4,N/2,3*N/4,N], ["$-\pi$","$-\pi/2$", "0","$\pi/2$", "$\pi$"],fontsize=fsize+10);
            else:
                plt.xticks([0,N/4,N/2,N-1], ["$0$","$2^{R-2}$", "$2^{R-1}$", "$2^R$"],fontsize=fsize);
            
            plt.yticks(fontsize=fsize);
            i += 1;
        plt.legend(leg, fontsize=fsize, bbox_to_anchor=(0., 1.02, 1., .102), loc=3,ncol=3, borderaxespad=0., mode='expand');
        
        
        filename = dest + "%s_block%d.png" % (name,k);
        plt.savefig(filename, dpi=150,transparent=False, bbox_inches='tight');
        k += 1;
    
    
if __name__ == "__main__":
    vm_list = [1,4,10];
    nu = 10
    samp_op = lambda x: fft.fftshift(fft.fft(x))/sqrt(2**nu);    
    name = 'fourier';
    plot_matrices_and_blocs(samp_op, vm_list, nu,  True);
    
    samp_op = lambda x: sqrt(2**nu)*fastwht(x);
    plot_matrices_and_blocs(samp_op, vm_list, nu,  False);
    
    
    
